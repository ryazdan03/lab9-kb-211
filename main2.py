import csv
import random
import matplotlib.pyplot as plt

x = []
for i in range(1, 11):
    x.append(random.randrange(0, 20))
    i += i
x.sort()

y = []
for i in range(1, 11):
    y.append(random.randrange(0, 20))
    i += i
y.sort()

filename = "xy.csv"

with open(filename, "w", newline="") as file:
    writer = csv.writer(file)
    writer.writerow(x)
    writer.writerow(y)

with open(filename, "r", newline="") as file:
    array = file.read()
    array = x
    print(x, end="")
    array = file.read()
    array = y
    print(y)

a = sum(x)/len(x)
b = sum(y)/len(y)

fig, ax = plt.subplots()
ax.plot(x, y, marker = 'o')
ax.plot(a, b, marker = 'o')
plt.show()
