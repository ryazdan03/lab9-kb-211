import random
import csv

x = []
for i in range(1, 11):
    x.append(random.randrange(0, 20))
    i += i
print(x)

y = []
for i in range(1, 11):
    y.append(random.randrange(0, 20))
    i += i
print(y)

filename = "xy.csv"

with open(filename, "w", newline="") as file:
    writer = csv.writer(file)
    writer.writerow(x)
    writer.writerow(y)

with open(filename, "r", newline="") as file:
    x1 = file.readline()
    print(x1, end="")
    y1 = file.readline()
    print(y1)

